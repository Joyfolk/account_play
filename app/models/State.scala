package models

import java.time.LocalDate

import play.api.libs.json.{Json, OWrites, Reads}

case class State(balance: BigDecimal, accessDay: LocalDate, withdrawalCount: Int, depositCount: Int,
                 withdrawalSum: BigDecimal, depositSum: BigDecimal) {

  import State._

  def withdraw(amount: BigDecimal, now: LocalDate): Either[ValidationError, State] = {
    processDate(now) flatMap {
      ns =>
        if (amount <= 0)
          Left(AmountShouldBePositive)
        else if (balance < amount)
          Left(BalanceLessThanWithdrawalAmount)
        else if (withdrawalCount >= MAX_WITHDRAWAL_FREQUENCY)
          Left(ExceededMaximumWithdrawalFrequency)
        else if (amount > MAX_WITHDRAWAL_PER_TRANSACTION)
          Left(ExceededMaximumWithdrawalPerTransaction)
        else if (amount + withdrawalSum > MAX_WITHDRAWAL_FOR_DAY)
          Left(ExceededMaximumWithdrawalForDay)
        else
          Right(ns.copy(
            balance = ns.balance - amount,
            withdrawalSum = ns.withdrawalSum + amount,
            withdrawalCount = ns.withdrawalCount + 1
          ))
    }
  }

  def deposit(amount: BigDecimal, now: LocalDate): Either[ValidationError, State] =
    processDate(now) flatMap {
      ns =>
        if (amount <= 0)
          Left(AmountShouldBePositive)
        else if (depositCount >= MAX_DEPOSIT_FREQUENCY)
          Left(ExceededMaximumDepositFrequency)
        else if (amount > MAX_DEPOSIT_PER_TRANSACTION)
          Left(ExceededMaximumDepositPerTransaction)
        else if (amount + depositSum > MAX_DEPOSIT_FOR_DAY)
          Left(ExceededMaximumDepositForDay)
        else
          Right(ns.copy(
            balance = ns.balance + amount,
            depositSum = ns.depositSum + amount,
            depositCount = ns.depositCount + 1
          ))
    }

  private def processDate(now: LocalDate): Either[ValidationError, State] =
    if (accessDay.isAfter(now))
      Left(InvalidDate)
    else if (accessDay.isBefore(now))
      Right(State(balance, now, 0, 0, 0, 0))
    else
      Right(this)
}


object State {
  implicit val stateReads: Reads[State] = Json.reads[State]
  implicit val stateWrites: OWrites[State] = Json.writes[State]

  def parse(str: String): State = Json.fromJson(Json.parse(str)).get
  def write(state: State): String = Json.toJson(state).toString()

  val MAX_WITHDRAWAL_FREQUENCY = 3
  val MAX_WITHDRAWAL_FOR_DAY: BigDecimal = 50000
  val MAX_WITHDRAWAL_PER_TRANSACTION: BigDecimal = 20000

  val MAX_DEPOSIT_FREQUENCY = 4
  val MAX_DEPOSIT_FOR_DAY: BigDecimal = 150000
  val MAX_DEPOSIT_PER_TRANSACTION: BigDecimal = 40000

  def init(now: LocalDate): State = State(0, now, 0, 0, 0, 0)
}