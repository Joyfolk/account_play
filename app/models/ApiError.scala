package models

import play.api.libs.json.{Json, Writes}

sealed trait ApiError {
  def message: String
}

final case class ExceptionError(throwable: Throwable) extends ApiError {
  def message: String = throwable.getLocalizedMessage
}

sealed trait ValidationError extends ApiError

case object InvalidDate extends ValidationError {
  override def message: String = "Invalid date"
}

sealed trait DepositError extends ValidationError
sealed trait WithdrawalError extends ValidationError

case object ExceededMaximumWithdrawalForDay extends WithdrawalError {
  override def message: String = "Exceeded maximum withdrawal for day"
}

case object ExceededMaximumWithdrawalPerTransaction extends WithdrawalError {
  override def message: String = "Exceeded maximum withdrawal per transaction"
}

case object ExceededMaximumWithdrawalFrequency extends WithdrawalError {
  override def message: String = "Exceeded maximum withdrawal frequency"
}

case object BalanceLessThanWithdrawalAmount extends WithdrawalError {
  override def message: String = "Balance is less than withdrawal amount"
}

case object AmountShouldBePositive extends ValidationError {
  override def message: String = "Amount should be positive"
}

case object ExceededMaximumDepositForDay extends WithdrawalError {
  override def message: String = "Exceeded maximum deposit for day"
}

case object ExceededMaximumDepositPerTransaction extends WithdrawalError {
  override def message: String = "Exceeded maximum deposit per transaction"
}

case object ExceededMaximumDepositFrequency extends WithdrawalError {
  override def message: String = "Exceeded maximum deposit frequency"
}

object ApiError {
  implicit val errorWriter: Writes[ApiError] = (o: ApiError) => Json.obj("error" -> o.message)
}