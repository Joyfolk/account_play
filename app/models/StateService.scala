package models

import java.io.{File, FileWriter, PrintWriter}
import java.time.LocalDate

import scala.io.Source
import scala.util.Try

trait StateService {
  def read(): Either[ApiError, State]
  def update(action: State => Either[ApiError, State]): Either[ApiError, State]
  def shutdown(): Unit = {}
}

object StateService {
  def forFile(file: File): StateService = new StateService {
    val lock = new Object
    var state: State = State.init(LocalDate.now())
    val writer = new PrintWriter(new FileWriter(file, true))

    override def read(): Either[ApiError, State] = Try {
      var res: State = null
      lock.synchronized {
        val now = LocalDate.now()
        res = Source.fromFile(file, "utf8").getLines().toStream.lastOption match {
          case None => State.init(now)
          case Some(str) => State.parse(str)
        }
      }
      res
    }.toEither.left.map(ExceptionError)


    override def update(action: State => Either[ApiError, State]): Either[ApiError, State] = {
      var res: Either[ApiError, State] = null
      lock.synchronized {
        res = action(state)
        try {
          res foreach {
            newState =>
              writer.println(State.write(newState))
              writer.flush()
              state = newState
          }
        }
        catch {
          case ex: Exception =>
            res = Left(ExceptionError(ex))
        }
      }
      res
    }

    override def shutdown(): Unit = {
      Try { writer.close() }
    }
  }

  def inMemory: StateService = new StateService {
    val lock = new Object
    var state: State = State.init(LocalDate.now())

    override def read(): Either[ApiError, State] = {
      var res: State = null
      lock.synchronized {
        res = state
      }
      Right(res)
    }

    override def update(action: State => Either[ApiError, State]): Either[ApiError, State] = {
      var res: Either[ApiError, State] = null
      lock.synchronized {
        res = action(state)
        res.foreach(newState => this.state = newState)
      }
      res
    }
  }
}
