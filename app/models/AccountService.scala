package models

import java.time.LocalDate

import com.google.inject.{Inject, Singleton}

import scala.concurrent.{ExecutionContext, Future}

@Singleton
class AccountService @Inject() (stateService: StateService)(implicit val ec: ExecutionContext) {
  def balance(): Future[Either[ApiError, BigDecimal]] =
    Future { stateService.read().map(_.balance) }

  def deposit(amount: BigDecimal): Future[Either[ApiError, BigDecimal]] = Future {
    val now = LocalDate.now()
    stateService.update(s => s.deposit(amount, now)).map(_.balance)
  }

  def withdrawal(amount: BigDecimal): Future[Either[ApiError, BigDecimal]] = Future {
    val now = LocalDate.now()
    stateService.update(s => s.withdraw(amount, now)).map(_.balance)
  }
}
