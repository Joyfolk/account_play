import java.io.File

import com.google.inject.{AbstractModule, Provides, Singleton}
import models.StateService
import play.api.{Configuration, Environment}

class Module(environment: Environment, configuration: Configuration) extends AbstractModule {
  override def configure(): Unit = {}

  @Provides
  @Singleton
  def stateService(): StateService = StateService.forFile(new File(configuration.get[String]("account.storage")))
}