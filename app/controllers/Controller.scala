package controllers

import java.text.DecimalFormat

import javax.inject._
import models.{AccountService, ApiError, ValidationError}
import play.api.libs.json.Json
import play.api.mvc._

import scala.concurrent.{ExecutionContextExecutor, Future}

class Controller @Inject() (cc: ControllerComponents, accountService: AccountService)
  extends AbstractController(cc) {

  implicit val ec: ExecutionContextExecutor = scala.concurrent.ExecutionContext.global

  def balance(): Action[AnyContent] = Action.async { implicit request: Request[AnyContent] =>
    accountService.balance().map {
      case Left(err) => InternalServerError(Json.toJson(err))
      case Right(balance) => success(balance)
    }
  }
  
  def deposit: Action[AnyContent] = Action.async { implicit request: Request[AnyContent] =>
    readAmount(request).map {
      amount =>
        accountService.deposit(amount).map {
          case Left(err: ValidationError) => Forbidden(Json.toJson(err))
          case Left(err: ApiError) => InternalServerError(Json.toJson(err))
          case Right(balance) => success(balance)
        }
    } getOrElse Future(badRequest)
  }

  def withdrawal: Action[AnyContent] = Action.async { implicit request: Request[AnyContent] =>
    readAmount(request).map {
      amount =>
        accountService.withdrawal(amount).map {
          case Left(err: ValidationError) => Forbidden(Json.toJson(err))
          case Left(err: ApiError) => InternalServerError(Json.toJson(err))
          case Right(balance) => success(balance)
        }
    } getOrElse Future(badRequest)
  }

  val decimalFormat = new DecimalFormat("###.##")

  def success(amount: BigDecimal) = Ok(Json.toJson(Map("balance" -> decimalFormat.format(amount))))
  val badRequest = BadRequest(Json.toJson(Map("result" -> "invalid json")))

  def readAmount(request: Request[AnyContent]): Option[BigDecimal] = 
    request.body.asJson.flatMap(json => (json \ "amount").asOpt[BigDecimal])
}
