package controllers

import models._
import org.scalatestplus.play._
import org.scalatestplus.play.guice._
import play.api.libs.json.Json
import play.api.test.Helpers._
import play.api.test._

import scala.concurrent.{Await, ExecutionContext, ExecutionContextExecutor}

class ControllerSpec extends PlaySpec with GuiceOneAppPerTest with Injecting {

  "Account" should {

    "be initialized with empty values" in {
      implicit val ec: ExecutionContextExecutor = ExecutionContext.global
      val accountService = new AccountService(StateService.inMemory)
      val controller = new Controller(stubControllerComponents(), accountService)
      val res = controller.balance().apply(FakeRequest(GET, "/"))
      status(res) mustBe OK
      contentType(res) mustBe Some("application/json")
      contentAsJson(res) mustEqual Json.obj("balance" -> "0")
    }

    "process deposit request" in {
      implicit val ec: ExecutionContextExecutor = ExecutionContext.global
      val accountService = new AccountService(StateService.inMemory)
      val controller = new Controller(stubControllerComponents(), accountService)
      val res = controller.deposit().apply(FakeRequest(POST, "/").withJsonBody(Json.obj("amount" -> "1000")))
      status(res) mustBe OK
      contentType(res) mustBe Some("application/json")
      contentAsJson(res) mustEqual Json.obj("balance" -> "1000")
    }

    "process withdrawal request" in {
      import scala.concurrent.duration._
      implicit val ec: ExecutionContextExecutor = ExecutionContext.global
      val accountService = new AccountService(StateService.inMemory)
      val controller = new Controller(stubControllerComponents(), accountService)
      Await.result(accountService.deposit(10000), 100 millis).isRight mustBe true
      val res = controller.withdrawal().apply(FakeRequest(POST, "/").withJsonBody(Json.obj("amount" -> "1000")))
      status(res) mustBe OK
      contentType(res) mustBe Some("application/json")
      contentAsJson(res) mustEqual Json.obj("balance" -> "9000")
    }

    "process deposit request errors" in {
      implicit val ec: ExecutionContextExecutor = ExecutionContext.global
      val accountService = new AccountService(StateService.inMemory)
      val controller = new Controller(stubControllerComponents(), accountService)
      val res = controller.deposit().apply(FakeRequest(POST, "/").withJsonBody(Json.obj("amount" -> "-1")))
      status(res) mustBe FORBIDDEN
      contentType(res) mustBe Some("application/json")
      contentAsJson(res) mustEqual Json.obj("error" -> "Amount should be positive")
    }

    "process withdrawal request errors" in {
      implicit val ec: ExecutionContextExecutor = ExecutionContext.global
      val accountService = new AccountService(StateService.inMemory)
      val controller = new Controller(stubControllerComponents(), accountService)
      val res = controller.withdrawal().apply(FakeRequest(POST, "/").withJsonBody(Json.obj("amount" -> "10")))
      status(res) mustBe FORBIDDEN
      contentType(res) mustBe Some("application/json")
      contentAsJson(res) mustEqual Json.obj("error" -> "Balance is less than withdrawal amount")
    }

    "process balance request errors" in {
      implicit val ec: ExecutionContextExecutor = ExecutionContext.global
      val accountService = new AccountService(new StateService {
        override def read(): Either[ApiError, State] = Left(models.ExceptionError(new Throwable("test")))
        override def update(action: State => Either[ApiError, State]): Either[ApiError, State] = null
      })
      val controller = new Controller(stubControllerComponents(), accountService)
      val res = controller.balance().apply(FakeRequest(GET, "/"))
      status(res) mustBe INTERNAL_SERVER_ERROR
      contentType(res) mustBe Some("application/json")
      contentAsJson(res) mustEqual Json.obj("error" -> "test")
    }
  }
}
