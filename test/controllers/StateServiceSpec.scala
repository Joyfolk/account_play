package controllers

import java.io.File
import java.time.LocalDate

import models.{BalanceLessThanWithdrawalAmount, StateService}
import org.scalatestplus.play.PlaySpec

class StateServiceSpec extends PlaySpec {
  "StateService.inMemory" should {

    "be initialized with initial state by default" in {
      val service = StateService.inMemory
      val state = service.read()
      state.isRight mustBe true
      state.right.get.balance mustBe 0
    }

    "be updated on request" in {
      val service = StateService.inMemory
      val now = LocalDate.now()
      val res = service.update(s => s.deposit(100, now))
      res.isRight mustBe true
      res.right.get.balance mustBe 100
      service.read() mustBe res
    }

    "correctly process sequence of updates" in {
      val service = StateService.inMemory
      val now = LocalDate.now()
      service.update(s => s.deposit(100, now))
      service.update(s => s.withdraw(50, now))
      service.update(s => s.deposit(20, now))
      val res = service.read()
      res.isRight mustBe true
      res.right.get.balance mustBe 70
    }

    "correctly process state update errors" in {
      val service = StateService.inMemory
      val now = LocalDate.now()
      service.update(s => s.deposit(100, now))
      val res = service.update(s => s.withdraw(150, now))
      res mustBe Left(BalanceLessThanWithdrawalAmount)
      val r = service.read()
      r.isRight mustBe true
      r.right.get.balance mustBe 100
    }
  }

  "StateService.forFile" should {

    "be initialized with initial state by default" in {
      val file = File.createTempFile("account.", ".test")
      file.deleteOnExit()
      val service = StateService.forFile(file)
      val state = service.read()
      state.isRight mustBe true
      state.right.get.balance mustBe 0
      service.shutdown()
    }

    "be updated on request" in {
      val file = File.createTempFile("account.", ".test")
      file.deleteOnExit()
      val service = StateService.forFile(file)
      val now = LocalDate.now()
      val res = service.update(s => s.deposit(100, now))
      res.isRight mustBe true
      res.right.get.balance mustBe 100
      service.read() mustBe res
      service.shutdown()
      val newService = StateService.forFile(file)
      newService.read() mustBe res
      newService.shutdown()
    }

    "correctly process sequence of updates" in {
      val file = File.createTempFile("account.", ".test")
      file.deleteOnExit()
      val service = StateService.forFile(file)
      val now = LocalDate.now()
      service.update(s => s.deposit(100, now))
      service.update(s => s.withdraw(50, now))
      service.update(s => s.deposit(20, now))
      val res = service.read()
      res.isRight mustBe true
      res.right.get.balance mustBe 70
      service.shutdown()
      val newService = StateService.forFile(file)
      newService.read() mustBe res
      newService.shutdown()
    }

    "correctly process state update errors" in {
      val file = File.createTempFile("account.", ".test")
      file.deleteOnExit()
      val service = StateService.forFile(file)
      val now = LocalDate.now()
      service.update(s => s.deposit(100, now))
      val res = service.update(s => s.withdraw(150, now))
      res mustBe Left(BalanceLessThanWithdrawalAmount)
      val r = service.read()
      r.isRight mustBe true
      r.right.get.balance mustBe 100
      service.shutdown()
      val newService = StateService.forFile(file)
      newService.read() mustBe r
      newService.shutdown()
    }
  }
}
