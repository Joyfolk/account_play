package controllers

import java.time.LocalDate

import models._
import org.scalatestplus.play._

class StateSpec extends PlaySpec {

  "State" should {

    "be initialized with zero balance" in {
      val now = LocalDate.now()
      val state = State.init(now)
      state mustBe State(0, now, 0, 0, 0, 0)
    }

    "should process deposit request and update counters" in {
      val now = LocalDate.now()
      val tomorrow = now.plusDays(1)
      val state = State(100, now, 1, 2, 100, 200)
      state.deposit(1000, now) mustBe Right(State(1100, now, 1, 3, 100, 1200))
      state.deposit(1000, tomorrow) mustBe Right(State(1100, tomorrow, 0, 1, 0, 1000))
    }

    "should process withdrawal request and update counters" in {
      val now = LocalDate.now()
      val tomorrow = now.plusDays(1)
      val state = State(1000, now, 1, 2, 100, 200)
      state.withdraw(500, now) mustBe Right(State(500, now, 2, 2, 600, 200))
      state.withdraw(500, tomorrow) mustBe Right(State(500, tomorrow, 1, 0, 500, 0))
    }

    "shouldn't process deposit request with non-positive amount" in {
      val now = LocalDate.now()
      val state = State.init(now)
      state.deposit(0, now) mustBe Left(AmountShouldBePositive)
      state.deposit(-1, now) mustBe Left(AmountShouldBePositive)
    }

    "shouldn't process withdrawal request with non-positive amount" in {
      val now = LocalDate.now()
      val state = State.init(now)
      state.withdraw(0, now) mustBe Left(AmountShouldBePositive)
      state.withdraw(-1, now) mustBe Left(AmountShouldBePositive)
    }

    "should check max deposit per transaction" in {
      val now = LocalDate.now()
      val state = State.init(now)
      state.deposit(40000.01, now) mustBe Left(ExceededMaximumDepositPerTransaction)
    }

    "should check max withdrawal per transaction" in {
      val now = LocalDate.now()
      val state = State(30000, now, 0, 0, 0, 0)
      state.withdraw(20000.01, now) mustBe Left(ExceededMaximumWithdrawalPerTransaction)
    }

    "should check max deposit per day" in {
      val now = LocalDate.now()
      val state = State(200000, now, 0, 1, 0, 140000)
      state.deposit(10000.01, now) mustBe Left(ExceededMaximumDepositForDay)
    }

    "should check max withdrawal per day" in {
      val now = LocalDate.now()
      val state = State(200000, now, 2, 0, 40000, 0)
      state.withdraw(10000.01, now) mustBe Left(ExceededMaximumWithdrawalForDay)
    }

    "should check max deposit frequency" in {
      val now = LocalDate.now()
      val state = State(100, now, 0, 3, 0, 100)
      state.deposit(100, now).isRight mustBe true
      state.deposit(100, now).flatMap(_.deposit(100, now)) mustBe Left(ExceededMaximumDepositFrequency)
    }

    "should check max withdrawal frequency" in {
      val now = LocalDate.now()
      val state = State(100, now, 2, 0, 100, 0)
      state.withdraw(10, now).isRight mustBe true
      state.withdraw(10, now).flatMap(_.withdraw(10, now)) mustBe Left(ExceededMaximumWithdrawalFrequency)
    }

    "should check balance before withdrawal" in {
      val now = LocalDate.now()
      val state = State(100, now, 0, 0, 0, 0)
      state.withdraw(100, now) mustBe Right(State(0, now, 1, 0, 100, 0))
      state.withdraw(100.01, now) mustBe Left(BalanceLessThanWithdrawalAmount)
    }

    "should not allow invalid request dates" in {
      val now = LocalDate.now()
      val yesterday = now.minusDays(1)
      val state = State(100, now, 0, 0, 0, 0)
      state.deposit(100, yesterday) mustBe Left(InvalidDate)
      state.withdraw(100, yesterday) mustBe Left(InvalidDate)
    }
  }
}
