### Building project

- `sbt test` to run tests
- `sbt coverageReport` for test coverage report 
- `sbt run` to run project

### Usage
```
curl localhost:9000/deposit -H "Content-type:application/json"  -X POST -d "{\"amount\": \"1000\"}"
>>> {"balance":"1000"}

curl localhost:9000/withdrawal -H "Content-type:application/json"  -X POST -d "{\"amount\": \"1500\"}"
>>> {"error":"Balance is less than withdrawal amount"}

curl localhost:9000/balance
>>> {"balance":"1000"}  
```
